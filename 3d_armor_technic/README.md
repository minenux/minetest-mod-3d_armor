minetest mod 3d_armor_technic
============================

ARMOR from procesed materials

Information
-----------

Adds armor made from lead, brass, cast iron, carbon steel, stainless steel, tin and silver.

![](screenshot.png)

Technical information
---------------------

This mod is named `3d_armor_technic` and is(was) part of the `3d_armor`, 
it was slited since version 0.4.11 of `3d_armor` now in minenux flavor merged again

Currently the mod was updated at https://github.com/mt-mods/technic_armor but this 
one already track the updates

#### Depends

* default
* 3d_armor
* technic (technic_worldgen)
* moreores

## LICENSE

Textures by @poet-nohit and @numberZero

Copyright (C) 2013-2018 Stuart Jones - LGPL v2.1

